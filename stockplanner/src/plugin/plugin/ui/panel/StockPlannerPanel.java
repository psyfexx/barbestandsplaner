package plugin.plugin.ui.panel;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import adapter.adapter.helper.ObjectToStringHelper;
import domain.domain.entity.Bar;
import domain.domain.entity.BeverageStorage;
import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;
import plugin.plugin.controller.BarController;
import plugin.plugin.ui.dialog.BeverageDialog;

public class StockPlannerPanel extends JFrame {

	private static final long serialVersionUID = -8774715635559382826L;
	private Bar bar;
	private HeaderPane headerPane;
	private BeverageControlPane beverageControlPane;
	private InformationPane infoPane;
	private FooterButtonPane footerPane;
	private BarController controller;

	public StockPlannerPanel(BarController controller, Bar bar) {
		super(bar.getName());
		this.controller = controller;
		this.bar = bar;
		initView();
	}

	private void initView() {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));

		headerPane = new HeaderPane(this, bar);
		beverageControlPane = new BeverageControlPane(this);
		infoPane = new InformationPane();
		footerPane = new FooterButtonPane(this);

		main.add(headerPane);
		main.add(beverageControlPane);
		main.add(infoPane);
		main.add(footerPane);
		checkLastWeekButton();

		add(main);
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void checkLastWeekButton() {
		if (!controller.doesLastWeekReportExist()) {
			headerPane.enableLastWeekButton(false);
		}
	}

	public Week getWeek() {
		return controller.getWeek();
	}

	public void showSelectedBeverage(BeverageStorage storage) {
		Count weekStock = controller.getWeeklyReport(storage).getWeekStock();
		String info;
		if (controller.getWeek().equals(Week.getCurrentWeek())) {
			info = ObjectToStringHelper.getBeverageInfoForCurrentWeek(weekStock, storage);
		} else {
			info = ObjectToStringHelper.getBeverageInfoForPastWeek(weekStock, storage);
		}
		infoPane.setText(info);
		beverageControlPane.resetSellCountTextField();
		beverageControlPane.enableComponents(true);
		footerPane.enableFillStockButton(false);
	}

	public void changeToNextWeek() {
		controller.changeWeek(true);
		infoPane.clearText();
		beverageControlPane.setOutOfFocus();
		footerPane.enableShoppingListButton(true);
		footerPane.enableFillStockButton(false);
		headerPane.enableLastWeekButton(true);
		headerPane.showCurrentWeek(controller.getWeek());
		if (controller.getWeek().equals(Week.getCurrentWeek())) {
			headerPane.enableNextWeekButton(false);
			beverageControlPane.enableCreateButton(true);
		}
	}

	public void changeToLastWeek() {
		controller.changeWeek(false);
		infoPane.clearText();
		beverageControlPane.setOutOfFocus();
		beverageControlPane.enableCreateButton(false);
		footerPane.enableShoppingListButton(false);
		checkLastWeekButton();
		headerPane.showCurrentWeek(controller.getWeek());
		headerPane.enableNextWeekButton(true);
	}

	public void createNewBeverage() {
		BeverageDialog dialogPane = new BeverageDialog(bar.getBarID());
		int result = JOptionPane.showConfirmDialog(beverageControlPane, dialogPane, "Create New Beverage",
				JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {
			BeverageStorage storage = dialogPane.getBeverageStorage();
			Count weeklyStock = dialogPane.getWeeklyStock();
			controller.createNewBeverage(storage, weeklyStock);
			beverageControlPane.addNewBeverage(storage);
		}
	}

	public void editCurrentBeverage() {
		BeverageStorage storage = beverageControlPane.getSelectedBeverageStorage();
		Count weeklyStock = controller.getWeeklyReport(storage).getWeekStock();
		Week week = controller.getWeek();
		BeverageDialog dialogPane = new BeverageDialog(storage, weeklyStock, week);
		int result = JOptionPane.showConfirmDialog(beverageControlPane, dialogPane,
				"Edit Beverage" + storage.getBeverage().getName(), JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {
			storage = dialogPane.getBeverageStorage();
			weeklyStock = dialogPane.getWeeklyStock();
			controller.updateBeverage(storage, weeklyStock);
			showSelectedBeverage(storage);
		}
	}

	public void fillStock() {
		List<BeverageStorage> storages = controller.fillStock();
		beverageControlPane.updateBeverageComboBox(storages);
	}

	public void sellSelectedBeverage() {
		BeverageStorage storage = beverageControlPane.getSelectedBeverage();
		int count = beverageControlPane.getSellCount();
		controller.sellBeverage(storage, count);
		showSelectedBeverage(storage);
		checkStockAmount(storage);
	}

	private void checkStockAmount(BeverageStorage storage) {
		WeeklyReport report = controller.getWeeklyReport(storage);
		if (storage.getStock().getValue() < Math.max(2, 0.15 * report.getWeekStock().getValue())) {
			JOptionPane.showMessageDialog(this, "Stock of " + storage.getBeverage().getName() + " is almost empty!");
		}
	}

	public void showShoppingList() {
		String list = controller.getShoppingList();
		infoPane.setText(list);
		beverageControlPane.setOutOfFocus();
		footerPane.enableFillStockButton(true);
	}

	public void showWeekEvaluation() {
		String evaluation = controller.getWeekEvaluation();
		infoPane.setText(evaluation);
		beverageControlPane.setOutOfFocus();
		footerPane.enableFillStockButton(false);
	}

	public List<BeverageStorage> getBeverages() {
		return controller.getBeverageStorages();
	}

}
