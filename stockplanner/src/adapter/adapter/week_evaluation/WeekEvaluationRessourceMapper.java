package adapter.adapter.week_evaluation;

import java.util.function.Function;

import application.application.dto.WeekEvaluationDTO;
import domain.domain.entity.Beverage;
import domain.domain.entity.WeeklyReport;
import domain.domain.entity.WeeklySales;

public class WeekEvaluationRessourceMapper implements Function<WeekEvaluationDTO, WeekEvaluationRessource> {

	@Override
	public WeekEvaluationRessource apply(WeekEvaluationDTO evaluation) {
		return map(evaluation);
	}

	private WeekEvaluationRessource map(WeekEvaluationDTO evaluation) {
		Beverage beverage = evaluation.getBeverage();
		WeeklyReport report = evaluation.getReport();
		WeeklySales sales = report.getWeeklySales();
		return new WeekEvaluationRessource(beverage.getName(), report.getWeekStock().getValue(),
				sales.getSoldCount().getValue(), evaluation.getRecommendedWeekStock().getValue());
	}

}
