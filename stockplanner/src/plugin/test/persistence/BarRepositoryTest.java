package plugin.test.persistence;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import domain.domain.entity.Bar;
import domain.domain.value_object.Name;
import plugin.plugin.persistence.BarRepositoryImp;

public class BarRepositoryTest {

	@Test
	public void testFindBarForID() {
		BarRepositoryImp repository = new BarRepositoryImp("./mockData/test");
		Bar bar = repository.findBarForID("barID");
		Object[] foundBar = new Object[] { bar.getBarID(), bar.getName(), bar.getOwner() };
		Object[] expectedBar = new Object[] { "barID", "Bar", new Name("Mr.", "Test", "Tester") };

		assertArrayEquals(expectedBar, foundBar);
	}

}
