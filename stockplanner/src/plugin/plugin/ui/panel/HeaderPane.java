package plugin.plugin.ui.panel;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import domain.domain.entity.Bar;
import domain.domain.value_object.Week;
import plugin.plugin.ui.listener.LastWeekListener;
import plugin.plugin.ui.listener.NextWeekListener;

public class HeaderPane extends JPanel {

	private static final long serialVersionUID = 3194957139033153135L;

	private JLabel ownerLabel;
	private JLabel nameLabel;
	private JLabel weekLabel;
	private JButton lastWeekButton;
	private JButton nextWeekButton;

	public HeaderPane(StockPlannerPanel stockPlannerPanel, Bar bar) {
		setSize(new Dimension(500, 50));
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		ownerLabel = new JLabel(bar.getOwner().toString());
		nameLabel = new JLabel(bar.getName());
		weekLabel = new JLabel(stockPlannerPanel.getWeek().toString());
		lastWeekButton = new JButton("<");
		nextWeekButton = new JButton(">");

		add(new JLabel("Owner: "));
		add(ownerLabel);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(new JLabel("Bar: "));
		add(nameLabel);
		add(Box.createRigidArea(new Dimension(20, 0)));
		add(new JLabel("Week: "));
		add(weekLabel);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(lastWeekButton);
		add(nextWeekButton);

		lastWeekButton.addActionListener(new LastWeekListener(stockPlannerPanel));
		nextWeekButton.addActionListener(new NextWeekListener(stockPlannerPanel));
		enableNextWeekButton(false);
	}

	public void enableNextWeekButton(boolean b) {
		nextWeekButton.setEnabled(b);
	}

	public void enableLastWeekButton(boolean b) {
		lastWeekButton.setEnabled(b);
	}

	public void showCurrentWeek(Week week) {
		weekLabel.setText(week.toString());

	}
}
