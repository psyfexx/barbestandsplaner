package domain.domain.entity;

import domain.domain.value_object.Count;

public class WeeklySales {
	private String id;
	private Count soldCount;

	protected WeeklySales(String id, Count soldCount) {
		this.id = id;
		this.soldCount = soldCount;
	}

	public String getId() {
		return id;
	}

	public Count getSoldCount() {
		return soldCount;
	}

	protected void setSoldCount(Count soldCount) {
		this.soldCount = soldCount;
	}

}
