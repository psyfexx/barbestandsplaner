package application.test.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.application.dto.ShoppingItemDTO;
import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;

public class ShoppingItemDTOTest {

	@Test
	public void testShoppingItemDTOTotalCost() {
		Beverage beverage = BeverageStorage.getBeverageInstance("testBeverageID", "testBarID", "Beverage",
				BeverageCategory.WATER, new Cost(1.50), new Cost(1.00));
		ShoppingItemDTO item = new ShoppingItemDTO(beverage, new Count(12));
		assertEquals(12.00, item.getTotalCost().getValue(), 0.001);
	}

}
