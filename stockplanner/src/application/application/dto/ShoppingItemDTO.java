package application.application.dto;

import domain.domain.entity.Beverage;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;

public final class ShoppingItemDTO {
	private final Beverage beverage;
	private final Count number;
	private final Cost totalCost;

	public ShoppingItemDTO(Beverage beverage, Count number) {
		super();
		this.beverage = beverage;
		this.number = number;
		this.totalCost = calculateTotalCost(beverage.getBuyingPrice(), number.getValue());
	}

	private Cost calculateTotalCost(Cost buyingPrice, int count) {
		double total = count * buyingPrice.getValue();
		return new Cost(total);
	}

	public Beverage getBeverage() {
		return beverage;
	}

	public Count getNumber() {
		return number;
	}

	public Cost getTotalCost() {
		return totalCost;
	}

}
