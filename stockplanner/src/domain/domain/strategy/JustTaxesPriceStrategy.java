package domain.domain.strategy;

import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;

public class JustTaxesPriceStrategy extends SellingPriceStrategy {

	@Override
	public Cost suggestSellingPrice(Cost buyingPrice, BeverageCategory category) {
		return getCostAfterTaxes(buyingPrice, category);
	}

}
