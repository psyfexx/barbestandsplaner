package application.test.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import application.application.dto.ShoppingItemDTO;
import application.application.service.use_case.ShowShoppingListService;
import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;

public class ShowShoppingListServiceTest {

	@Test
	public void testGetTotalCost() {
		Beverage beverage = BeverageStorage.getBeverageInstance("testBeverageID", "testBarID", "Beverage",
				BeverageCategory.WATER, new Cost(1.50), new Cost(1.00));
		ShoppingItemDTO item = new ShoppingItemDTO(beverage, new Count(12));
		Beverage beverage2 = BeverageStorage.getBeverageInstance("testBeverageID2", "testBarID2", "Beverage2",
				BeverageCategory.WINE, new Cost(1.50), new Cost(2.00));
		ShoppingItemDTO item2 = new ShoppingItemDTO(beverage2, new Count(15));
		List<ShoppingItemDTO> list = new ArrayList<>();
		list.add(item);
		list.add(item2);
		Cost totalCost = ShowShoppingListService.getTotalCost(list);
		assertEquals(42.00, totalCost.getValue(), 0.001);

	}

}
