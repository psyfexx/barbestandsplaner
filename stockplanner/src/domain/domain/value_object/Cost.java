package domain.domain.value_object;

import java.util.Objects;

public final class Cost {
	private final double value;
	private final char currency;

	public Cost(double value) {
		if (value < 0) {
			throw new IllegalArgumentException("Cost can't be less then zero");
		}
		this.value = Math.round(value * 100) / 100.0;
		this.currency = '\u20ac';
	}

	public static Cost subtractCost(Cost firstCost, Cost secondCost) {
		return new Cost(firstCost.getValue() - secondCost.getValue());
	}

	public double getValue() {
		return value;
	}

	public char getCurrency() {
		return currency;
	}

	@Override
	public String toString() {
		return String.format("%.02f", value) + currency;
	}

	@Override
	public int hashCode() {
		return Objects.hash(currency, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Cost))
			return false;
		Cost other = (Cost) obj;
		return currency == other.currency && Double.doubleToLongBits(value) == Double.doubleToLongBits(other.value);
	}

}
