# Stockplanner

This is the repository for the project stockplanner.
With this application you can track your sold beverages in your own bar. 
Take a look at past sales figures to keep track of your business.

## Manual Installation

Start the Application by executing the [StockPlannerApplication.java](https://gitlab.com/psyfexx/barbestandsplaner/-/blob/master/stockplanner/src/plugin/plugin/StockPlannerApplication.java) file in the plugin layer. 

## Personalized Usage
For a simple demo just use the MockBar database and MockBarID. 
Of course you can create your own Bar by starting a new database following this example. 
Don't forget to change the access file data in the [StockPlannerApplication.java](https://gitlab.com/psyfexx/barbestandsplaner/-/blob/master/stockplanner/src/plugin/plugin/StockPlannerApplication.java) file.

## Documentation
The technical documentation for this application can be found [here](https://gitlab.com/psyfexx/barbestandsplaner/-/blob/master/documents/Documentation.pdf). It is recommended to download this pdf to follow all given links.
