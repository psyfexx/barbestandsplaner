package plugin.plugin.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import plugin.plugin.ui.panel.StockPlannerPanel;

public class CreateBeverageListener implements ActionListener {

	private StockPlannerPanel stockPlannerPanel;

	public CreateBeverageListener(StockPlannerPanel stockPlannerPanel) {
		this.stockPlannerPanel = stockPlannerPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		stockPlannerPanel.createNewBeverage();

	}

}
