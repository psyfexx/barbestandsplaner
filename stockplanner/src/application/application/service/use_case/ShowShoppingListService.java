package application.application.service.use_case;

import java.util.ArrayList;
import java.util.List;

import application.application.dto.ShoppingItemDTO;
import application.application.service.BeverageInfoService;
import application.application.service.WeeklyReportService;
import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class ShowShoppingListService {
	private BeverageInfoService beverageInfoService;
	private WeeklyReportService reportService;

	public ShowShoppingListService(BeverageInfoService beverageInfoService, WeeklyReportService reportService) {
		super();
		this.beverageInfoService = beverageInfoService;
		this.reportService = reportService;
	}

	public List<ShoppingItemDTO> getShoppingList(String barID) {
		List<ShoppingItemDTO> shoppingList = new ArrayList<>();
		Week commingWeek = Week.getCommingWeek();
		for (BeverageStorage storage : beverageInfoService.getAllBeverageStorages(barID)) {
			ShoppingItemDTO shoppingItem = getShoppingItem(barID, reportService, storage, commingWeek);
			shoppingList.add(shoppingItem);
		}
		return shoppingList;
	}

	public static Cost getTotalCost(List<ShoppingItemDTO> items) {
		double totalCost = 0;
		for (ShoppingItemDTO item : items) {
			totalCost += item.getTotalCost().getValue();
		}
		return new Cost(totalCost);
	}

	private ShoppingItemDTO getShoppingItem(String barID, WeeklyReportService reportService, BeverageStorage storage,
			Week commingWeek) {
		Beverage beverage = storage.getBeverage();
		WeeklyReport report = reportService.getWeeklyReport(barID, beverage.getId(), commingWeek);
		int amount = report.getWeekStock().getValue() - storage.getStock().getValue();
		if (amount < 0)
			amount = 0;
		return new ShoppingItemDTO(beverage, new Count(amount));
	}

}
