package domain.domain.entity;

import java.util.UUID;

import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;

public class BeverageStorage {
	private String id;
	private String barID;
	private Count stock;
	private Beverage beverage;

	public BeverageStorage(String id, String barID, Count stock, Beverage beverage) {
		this.id = id;
		this.barID = barID;
		this.stock = stock;
		this.beverage = beverage;
	}

	public Count getStock() {
		return stock;
	}

	public void setStock(Count stock) {
		this.stock = stock;
	}

	public String getId() {
		return id;
	}

	public String getBarID() {
		return barID;
	}

	public Beverage getBeverage() {
		return beverage;
	}

	public void setBeverage(Beverage beverage) {
		this.beverage = beverage;
	}

	public static BeverageStorage createBeverageStorage(String barID, Count stock, String beverageName,
			BeverageCategory type, Cost buyingPrice, Cost sellingPrice) {
		String storageID = UUID.randomUUID().toString();
		Beverage beverage = createBeverage(barID, beverageName, type, sellingPrice, buyingPrice);
		return new BeverageStorage(storageID, barID, stock, beverage);
	}

	public static Beverage getBeverageInstance(String beverageID, String barID, String name, BeverageCategory type,
			Cost sellingPrice, Cost buyingPrice) {
		return new Beverage(beverageID, barID, name, sellingPrice, buyingPrice, type);
	}

	private static Beverage createBeverage(String barID, String name, BeverageCategory type, Cost sellingPrice,
			Cost buyingPrice) {
		String id = UUID.randomUUID().toString();
		return new Beverage(id, barID, name, sellingPrice, buyingPrice, type);
	}

	@Override
	public String toString() {
		return beverage.getName();
	}

}
