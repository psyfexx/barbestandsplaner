package plugin.plugin.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.domain.entity.Bar;
import domain.domain.repository.BarRepository;
import domain.domain.value_object.Name;
import plugin.plugin.controller.DBController;

public class BarRepositoryImp implements BarRepository {

	private DBController controller;

	public BarRepositoryImp(String fileLocation) {
		this.controller = new DBController(fileLocation + "_bar.sqlite");
		try {
			controller.executeUpdateSQL(
					"CREATE TABLE IF NOT EXISTS Bar (id VARCHAR(250) NOT NULL, name VARCHAR(250), owner_title VARCHAR(100), owner_prename VARCHAR(250), owner_surname VARCHAR(250), PRIMARY KEY (id));");
		} catch (SQLException e) {
			throw new RuntimeException("Table Bar could not be created");
		}
	}

	@Override
	public Bar findBarForID(String barID) {
		Bar bar = null;
		String sql = "SELECT * FROM Bar WHERE id = '" + barID + "';";
		ResultSet rs = controller.executeSQL(sql);
		try {
			if (rs.next()) {
				String name = rs.getString("name");
				String ownerTitle = rs.getString("owner_title");
				String ownerPreName = rs.getString("owner_prename");
				String ownerSurName = rs.getString("owner_surname");
				Name owner = new Name(ownerTitle, ownerPreName, ownerSurName);
				bar = new Bar(barID, name, owner);
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw new RuntimeException("id " + barID + "not found in table Bar");
		}
		controller.closeConnection();
		return bar;
	}

}
