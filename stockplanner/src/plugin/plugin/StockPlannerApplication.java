package plugin.plugin;

import domain.domain.entity.Bar;
import domain.domain.repository.BarRepository;
import domain.domain.repository.BeverageStorageRepository;
import domain.domain.repository.WeeklyReportRepository;
import domain.domain.value_object.Week;
import plugin.plugin.controller.BarController;
import plugin.plugin.persistence.BarRepositoryImp;
import plugin.plugin.persistence.BeverageStorageRepositoryImpl;
import plugin.plugin.persistence.WeeklyReportRepositoryImpl;
import plugin.plugin.ui.panel.StockPlannerPanel;

public class StockPlannerApplication {

	public static void main(String[] args) {
		String barID = "mockBarID";
		String mockLocation = "./mockData/mock";
		BarRepository barRepository = new BarRepositoryImp(mockLocation);
		BeverageStorageRepository storageRepository = new BeverageStorageRepositoryImpl(mockLocation);
		WeeklyReportRepository reportRepository = new WeeklyReportRepositoryImpl(mockLocation);
		Week currentWeek = Week.getCurrentWeek();
		BarController controller = new BarController(barID, barRepository, storageRepository, reportRepository,
				currentWeek);
		Bar bar = controller.getBar();
		if (bar != null) {
			StockPlannerPanel mainPanel = new StockPlannerPanel(controller, bar);
			mainPanel.setVisible(true);
		}
	}

}
