package domain.domain.entity;

import domain.domain.value_object.Name;

public class Bar {
	private String id;
	private String name;
	private Name owner;

	public Bar(String barID, String barName, Name owner) {
		this.id = barID;
		this.name = barName;
		this.owner = owner;
	}

	public String getBarID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Name getOwner() {
		return owner;
	}
}
