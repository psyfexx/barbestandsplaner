package domain.domain.strategy;

import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;

public class ToGoPriceStrategy extends SellingPriceStrategy {

	@Override
	public Cost suggestSellingPrice(Cost buyingPrice, BeverageCategory category) {
		Cost afterTaxes = getCostAfterTaxes(buyingPrice, category);
		return new Cost(afterTaxes.getValue() * 2);
	}

}
