package plugin.plugin.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.enumeration.BeverageCategory;
import domain.domain.repository.BeverageStorageRepository;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;
import plugin.plugin.controller.DBController;

public class BeverageStorageRepositoryImpl implements BeverageStorageRepository {
	private DBController controller;

	public BeverageStorageRepositoryImpl(String fileLocation) {
		this.controller = new DBController(fileLocation + "_bar.sqlite");
		try {
			controller.executeUpdateSQL(
					"CREATE TABLE IF NOT EXISTS BeverageStorage(id VARCHAR(250) NOT NULL, bar_id VARCHAR(250) NOT NULL, stock INTEGER, PRIMARY KEY (id), FOREIGN KEY(bar_id) REFERENCES Bar(id) ON DELETE CASCADE);");
			controller.executeUpdateSQL(
					"CREATE TABLE IF NOT EXISTS Beverage (id VARCHAR(250) NOT NULL, bar_id VARCHAR(250) NOT NULL, storage_id VARCHAR(250) NOT NULL, name VARCHAR(250), selling_price DECIMAL(5,2), buying_price DECIMAL(5,2), category VARCHAR(250), PRIMARY KEY (id), FOREIGN KEY(bar_id) REFERENCES Bar(id) ON DELETE CASCADE, FOREIGN KEY(storage_id) REFERENCES BeverageStorage(id) ON DELETE CASCADE);");
		} catch (SQLException e) {
			throw new RuntimeException("Tables BeverageStorage and Beverage could not be created");
		}
	}

	@Override
	public List<BeverageStorage> findAllBeverageStorages(String barID) {
		List<BeverageStorage> storages = new ArrayList<>();
		String sql = "SELECT s.id, s.stock, b.id, b.name, b.selling_price, b.buying_price, b.category FROM BeverageStorage s, Beverage b WHERE s.bar_id = '"
				+ barID + "' AND s.id = b.storage_id;";
		ResultSet rs = controller.executeSQL(sql);
		try {
			while (rs.next()) {
				String storageID = rs.getString(1);
				Count stock = new Count(rs.getInt(2));
				String beverageID = rs.getString(3);
				String name = rs.getString(4);
				Cost sellingPrice = new Cost(rs.getBigDecimal(5).doubleValue());
				Cost buyingPrice = new Cost(rs.getBigDecimal(6).doubleValue());
				BeverageCategory category = BeverageCategory.getCategoryForName(rs.getString(7));
				Beverage beverage = BeverageStorage.getBeverageInstance(beverageID, barID, name, category, sellingPrice,
						buyingPrice);
				BeverageStorage storage = new BeverageStorage(storageID, barID, stock, beverage);
				storages.add(storage);
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw new RuntimeException("no BeverageStorages found vor bar-id " + barID);
		}
		controller.closeConnection();
		return storages;
	}

	@Override
	public void addNewBeverageStorage(BeverageStorage storage) {
		Beverage beverage = storage.getBeverage();
		try {
			String sql = "INSERT INTO BeverageStorage(id, bar_id, stock) VALUES ('" + storage.getId() + "','"
					+ storage.getBarID() + "'," + storage.getStock().getValue() + ");";
			controller.executeUpdateSQL(sql);
			sql = "INSERT INTO Beverage(id, bar_id, storage_id, name, selling_price, buying_price, category) VALUES('"
					+ beverage.getId() + "','" + beverage.getBarId() + "','" + storage.getId() + "','"
					+ beverage.getName() + "'," + beverage.getSellingPrice().getValue() + ","
					+ beverage.getBuyingPrice().getValue() + ",'" + beverage.getCategory().getName() + "');";
			controller.executeUpdateSQL(sql);
		} catch (SQLException e) {
			throw new RuntimeException("Adding BeverageStorage failed");
		}
	}

	@Override
	public boolean updateBeverageStorage(BeverageStorage storage) {
		Beverage beverage = storage.getBeverage();
		boolean success = true;
		try {
			String sql = "UPDATE BeverageStorage SET stock =" + storage.getStock().getValue() + " WHERE id ='"
					+ storage.getId() + "';";
			controller.executeUpdateSQL(sql);
			sql = "UPDATE Beverage SET name = '" + beverage.getName() + "', selling_price = "
					+ beverage.getSellingPrice().getValue() + ", buying_price= " + beverage.getBuyingPrice().getValue()
					+ ", category='" + beverage.getCategory().getName() + "' WHERE id='" + beverage.getId() + "';";
			controller.executeUpdateSQL(sql);
		} catch (SQLException e) {
			success = false;
			System.out.println("Updating BeverageStorage failed");
		}
		return success;
	}

	@Override
	public boolean doesBeverageStorageWithSameNameAndTypeExist(BeverageStorage storage) {
		Beverage beverage = storage.getBeverage();
		boolean found = false;
		String sql = "SELECT * FROM Beverage WHERE bar_id = '" + storage.getBarID() + "' AND name = '"
				+ beverage.getName() + "'AND category = '" + beverage.getCategory().getName() + "';";
		ResultSet rs = controller.executeSQL(sql);
		try {
			if (rs.next()) {
				found = true;
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw (new RuntimeException(e));
		}
		controller.closeConnection();
		return found;
	}

	@Override
	public boolean updateStockForBeverageStorage(BeverageStorage storage) {
		boolean success = true;
		try {
			String sql = "UPDATE BeverageStorage SET stock =" + storage.getStock().getValue() + " WHERE id ='"
					+ storage.getId() + "';";
			controller.executeUpdateSQL(sql);
		} catch (SQLException e) {
			success = false;
			System.out.println("Updating Stock failed");
		}
		return success;
	}

	@Override
	public Beverage findBeverageForID(String beverageID, String barID) {
		Beverage beverage = null;
		String sql = "SELECT name, selling_price, buying_price, category FROM Beverage WHERE id = '" + beverageID
				+ "'AND bar_id ='" + barID + "';";
		ResultSet rs = controller.executeSQL(sql);
		try {
			if (rs.next()) {
				String name = rs.getString(1);
				Cost sellingPrice = new Cost(rs.getBigDecimal(2).doubleValue());
				Cost buyingPrice = new Cost(rs.getBigDecimal(3).doubleValue());
				BeverageCategory category = BeverageCategory.getCategoryForName(rs.getString(4));
				beverage = BeverageStorage.getBeverageInstance(beverageID, barID, name, category, sellingPrice,
						buyingPrice);
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw new RuntimeException("no Beverage found vor bar-id " + barID + " and beverageID " + beverageID);
		}
		controller.closeConnection();
		return beverage;
	}

}
