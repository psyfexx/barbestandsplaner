package plugin.plugin.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBController {
	private final String dbPath;

	private Connection connection;

	private Statement statement;

	public DBController(String dbPath) {
		this.dbPath = dbPath;
		this.statement = null;
		startConnection();
	}

	private void startConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			System.out.println("Fehler beim Laden des JDBC-Treibers");
			e.printStackTrace();
		}
		try {
			if (connection != null && !connection.isClosed()) {
				return;
			}
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
		} catch (SQLException e) {
			System.out.println("Fehler bei Pfadanalyse");
			e.printStackTrace();
		}
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				if (!connection.isClosed() && connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}));
	}

	public ResultSet executeSQL(String sql) {
		startConnection();
		try {
			statement = connection.createStatement();
			return statement.executeQuery(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void executeUpdateSQL(String sql) throws SQLException {
		startConnection();
		statement = connection.createStatement();
		statement.executeUpdate(sql);
		statement.close();
		closeConnection();
	}

	public void closeConnection() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
