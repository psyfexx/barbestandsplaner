package domain.domain.repository;

import java.util.List;

import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public interface WeeklyReportRepository {

	public List<WeeklyReport> findAllWeeklyReports(String barID, Week week);

	public WeeklyReport findWeeklyReport(String barID, String beverageID, Week week);

	public void addWeeklyReport(WeeklyReport newReport);

	public void updateWeeklySales(String salesID, Count soldCount);

	public void updateWeekReport(WeeklyReport report);

	public boolean doesAnyWeekReportExist(String barID, Week week);

}
