package domain.domain.entity;

import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;

public class Beverage {
	private String id;
	private String barID;
	private String name;
	private Cost sellingPrice;
	private Cost buyingPrice;
	private BeverageCategory category;

	protected Beverage(String id, String barID, String name, Cost sellingPrice, Cost buyingPrice,
			BeverageCategory category) {
		this.id = id;
		this.barID = barID;
		this.name = name;
		this.sellingPrice = sellingPrice;
		this.buyingPrice = buyingPrice;
		this.category = category;
	}

	public String getId() {
		return id;
	}

	public String getBarId() {
		return barID;
	}

	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public Cost getSellingPrice() {
		return sellingPrice;
	}

	protected void setSellingPrice(Cost sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Cost getBuyingPrice() {
		return buyingPrice;
	}

	protected void setBuyingPrice(Cost buyingPrice) {
		this.buyingPrice = buyingPrice;
	}

	public BeverageCategory getCategory() {
		return category;
	}

	protected void setCategory(BeverageCategory category) {
		this.category = category;
	}

}
