package plugin.test.persistence;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;
import plugin.plugin.persistence.BeverageStorageRepositoryImpl;

public class BeverageStorageRepositoryTest {

	private BeverageStorageRepositoryImpl repository;

	@Before
	public void setup() {
		repository = new BeverageStorageRepositoryImpl("./mockData/test");
	}

	@Test
	public void testAddNewBeverageStorage() {
		Random random = new Random();
		int rand = random.nextInt(1000);
		Beverage beverage = BeverageStorage.getBeverageInstance("beverage" + rand + "ID", "barID", "Beverage" + rand,
				BeverageCategory.WINE, new Cost(rand + 0.5), new Cost(rand + 1.5));
		BeverageStorage storage = new BeverageStorage("storage" + rand + "ID", "barID", new Count(rand), beverage);
		repository.addNewBeverageStorage(storage);
		boolean success = false;
		for (BeverageStorage strg : repository.findAllBeverageStorages("barID")) {
			if (strg.getId().equals("storage" + rand + "ID")) {
				success = true;
			}
		}
		assertEquals(true, success);
	}

	@Test
	public void testUpdateStockForBeverageStorage() {
		Beverage beverage = repository.findBeverageForID("beverageID", "barID");
		BeverageStorage storage = new BeverageStorage("storageID", "barID", new Count(50), beverage);
		repository.updateStockForBeverageStorage(storage);
		BeverageStorage changedStorage = null;
		for (BeverageStorage strg : repository.findAllBeverageStorages("barID")) {
			if (strg.getId().equals("storageID")) {
				changedStorage = strg;
			}
		}
		if (changedStorage != null) {
			assertEquals(new Count(50), changedStorage.getStock());
		} else {
			fail();
		}
	}

	@Test
	public void testFindBeverageForID() {
		Beverage beverage = repository.findBeverageForID("beverageID", "barID");
		Object[] foundBeverage = new Object[] { beverage.getId(), beverage.getBarId(), beverage.getName(),
				beverage.getBuyingPrice(), beverage.getSellingPrice(), beverage.getCategory() };
		Object[] expectedBeverage = new Object[] { "beverageID", "barID", "TestBeverage", new Cost(1.00),
				new Cost(1.50), BeverageCategory.BEER };

		assertArrayEquals(expectedBeverage, foundBeverage);
	}

}
