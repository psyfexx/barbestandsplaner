package application.application.dto;

import domain.domain.entity.Beverage;
import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Count;

public final class WeekEvaluationDTO {
	private final Beverage beverage;
	private final WeeklyReport report;
	private final Count recommendedWeekStock;

	public WeekEvaluationDTO(Beverage beverage, WeeklyReport report, Count recommendedWeekStock) {
		super();
		this.beverage = beverage;
		this.report = report;
		this.recommendedWeekStock = recommendedWeekStock;
	}

	public Beverage getBeverage() {
		return beverage;
	}

	public WeeklyReport getReport() {
		return report;
	}

	public Count getRecommendedWeekStock() {
		return recommendedWeekStock;
	}

}
