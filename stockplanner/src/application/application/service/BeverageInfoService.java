package application.application.service;

import java.util.List;

import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.repository.BeverageStorageRepository;

public class BeverageInfoService {
	private BeverageStorageRepository storageRepository;

	public BeverageInfoService(BeverageStorageRepository storageRepository) {
		this.storageRepository = storageRepository;
	}

	public void updateStockForBeverageStorage(BeverageStorage storage) {
		storageRepository.updateStockForBeverageStorage(storage);
	}

	public boolean doesBeverageStorageWithSameNameAndTypeExist(BeverageStorage storage) {
		return storageRepository.doesBeverageStorageWithSameNameAndTypeExist(storage);
	}

	public void addNewBeverageStorage(BeverageStorage storage) {
		storageRepository.addNewBeverageStorage(storage);
	}

	public List<BeverageStorage> getAllBeverageStorages(String barID) {
		return storageRepository.findAllBeverageStorages(barID);
	}

	public boolean updateBeverageStorage(BeverageStorage storage) {
		return storageRepository.updateBeverageStorage(storage);
	}

	public Beverage getBeverageForID(String beverageID, String barID) {
		return storageRepository.findBeverageForID(beverageID, barID);
	}

}
