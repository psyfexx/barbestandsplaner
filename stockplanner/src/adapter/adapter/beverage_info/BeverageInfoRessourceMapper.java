package adapter.adapter.beverage_info;

import java.util.function.Function;

import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.value_object.Cost;

public class BeverageInfoRessourceMapper implements Function<BeverageStorage, BeverageInfoRessource> {

	@Override
	public BeverageInfoRessource apply(BeverageStorage storage) {
		return map(storage);
	}

	private BeverageInfoRessource map(BeverageStorage storage) {
		Beverage beverage = storage.getBeverage();
		Cost buyingPrice = beverage.getBuyingPrice();
		Cost sellingPrice = beverage.getSellingPrice();
		return new BeverageInfoRessource(storage.getStock().getValue(), buyingPrice.getValue(), sellingPrice.getValue(),
				buyingPrice.toString(), sellingPrice.toString(), beverage.getName(), beverage.getCategory().getName());
	}
}
