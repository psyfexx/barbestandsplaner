package domain.domain.repository;

import java.util.List;

import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;

public interface BeverageStorageRepository {

	public List<BeverageStorage> findAllBeverageStorages(String barID);

	public void addNewBeverageStorage(BeverageStorage storage);

	public boolean updateBeverageStorage(BeverageStorage storage);

	public boolean doesBeverageStorageWithSameNameAndTypeExist(BeverageStorage storage);

	public boolean updateStockForBeverageStorage(BeverageStorage storage);

	public Beverage findBeverageForID(String beverageID, String barID);

}
