package domain.domain.repository;

import domain.domain.entity.Bar;

public interface BarRepository {

	public Bar findBarForID(String barID);

}
