package domain.domain.entity;

import java.util.UUID;

import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class WeeklyReport {
	private String id;
	private String beverageID;
	private String barID;
	private Count weekStock;
	private Week week;
	private WeeklySales weeklySales;

	public WeeklyReport(String id, String beverageID, String barID, Count weekStock, Week week,
			WeeklySales weeklySales) {
		this.id = id;
		this.beverageID = beverageID;
		this.barID = barID;
		this.weekStock = weekStock;
		this.week = week;
		this.weeklySales = weeklySales;
	}

	public Count getWeekStock() {
		return weekStock;
	}

	public Week getWeek() {
		return week;
	}

	public void setWeekStock(Count weekStock) {
		this.weekStock = weekStock;
	}

	public String getId() {
		return id;
	}

	public String getBeverageID() {
		return beverageID;
	}

	public String getBarID() {
		return barID;
	}

	public WeeklySales getWeeklySales() {
		return weeklySales;
	}

	public static WeeklySales getWeeklySalesInstance(String salesID, Count soldCount) {
		return new WeeklySales(salesID, soldCount);
	}

	public static WeeklyReport createCopyForNextWeek(WeeklyReport report) {
		String salesID = UUID.randomUUID().toString();
		String reportID = UUID.randomUUID().toString();
		Week commingWeek = Week.getCommingWeek();
		WeeklySales newSales = new WeeklySales(salesID, new Count(0));
		return new WeeklyReport(reportID, report.getBeverageID(), report.getBarID(), report.getWeekStock(), commingWeek,
				newSales);
	}

	public static WeeklyReport createWeeklyReport(String barID, String beverageID, Week week, Count weekStock) {
		String reportID = UUID.randomUUID().toString();
		String salesID = UUID.randomUUID().toString();
		WeeklySales sales = new WeeklySales(salesID, new Count(0));
		return new WeeklyReport(reportID, beverageID, barID, weekStock, week, sales);
	}

	public void addToWeeklySales(int soldCount) {
		Count newSalesCount = new Count(weeklySales.getSoldCount().getValue() + soldCount);
		weeklySales.setSoldCount(newSalesCount);
	}

}
