package domain.domain.enumeration;

public enum PriceMode {
	JUST_TAXES("Just Taxes"), WITH_PROFIT("With Profit"), TO_GO("To Go");

	private String mode;

	private PriceMode(String mode) {
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	@Override
	public String toString() {
		return mode;
	}
}
