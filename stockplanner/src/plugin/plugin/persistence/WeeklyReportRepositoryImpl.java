package plugin.plugin.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.domain.entity.WeeklyReport;
import domain.domain.entity.WeeklySales;
import domain.domain.repository.WeeklyReportRepository;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;
import plugin.plugin.controller.DBController;

public class WeeklyReportRepositoryImpl implements WeeklyReportRepository {

	private DBController controller;

	public WeeklyReportRepositoryImpl(String fileLocation) {
		this.controller = new DBController(fileLocation + "_bar.sqlite");
		try {
			controller.executeUpdateSQL(
					"CREATE TABLE IF NOT EXISTS WeeklyReport(id VARCHAR(250) NOT NULL, bar_id VARCHAR(250) NOT NULL, beverage_id VARCHAR(250) NOT NULL, week_stock INTEGER, year_week INTEGER, year INTEGER, PRIMARY KEY (id), FOREIGN KEY(bar_id) REFERENCES Bar(id) ON DELETE CASCADE, FOREIGN KEY(beverage_id) REFERENCES Beverage(id) ON DELETE CASCADE);");
			controller.executeUpdateSQL(
					"CREATE TABLE IF NOT EXISTS WeeklySales (id VARCHAR(250) NOT NULL, report_id VARCHAR(250) NOT NULL, sales_count INTEGER, PRIMARY KEY (id), FOREIGN KEY(report_id) REFERENCES WeeklyReport(id) ON DELETE CASCADE);");
		} catch (SQLException e) {
			throw new RuntimeException("Tables WeeklyReport and WeeklySales could not be created");
		}

	}

	@Override
	public List<WeeklyReport> findAllWeeklyReports(String barID, Week week) {
		List<WeeklyReport> reports = new ArrayList<>();
		String sql = "SELECT r.id, r.beverage_id, r.week_stock, s.id, s.sales_count FROM WeeklyReport r, WeeklySales s WHERE r.bar_id = '"
				+ barID + "' AND r.year_week =" + week.getYearWeek() + " AND r.year = " + week.getYear()
				+ " AND r.id = s.report_id;";
		ResultSet rs = controller.executeSQL(sql);
		try {
			while (rs.next()) {
				String reportID = rs.getString(1);
				String beverageID = rs.getString(2);
				Count weekStock = new Count(rs.getInt(3));
				String salesID = rs.getString(4);
				Count salesCount = new Count(rs.getInt(5));
				WeeklySales sales = WeeklyReport.getWeeklySalesInstance(salesID, salesCount);
				WeeklyReport report = new WeeklyReport(reportID, beverageID, barID, weekStock, week, sales);
				reports.add(report);
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw new RuntimeException("no WeeklyReport found vor bar-id " + barID + " and week " + week.toString());
		}
		controller.closeConnection();
		return reports;
	}

	@Override
	public WeeklyReport findWeeklyReport(String barID, String beverageID, Week week) {
		WeeklyReport report = null;
		String sql = "SELECT r.id, r.week_stock, s.id, s.sales_count FROM WeeklyReport r, WeeklySales s WHERE r.bar_id = '"
				+ barID + "' AND r.year_week =" + week.getYearWeek() + " AND r.year = " + week.getYear()
				+ " AND r.beverage_id ='" + beverageID + "' AND r.id = s.report_id;";
		ResultSet rs = controller.executeSQL(sql);
		try {
			if (rs.next()) {
				String reportID = rs.getString(1);
				Count weekStock = new Count(rs.getInt(2));
				String salesID = rs.getString(3);
				Count salesCount = new Count(rs.getInt(4));
				WeeklySales sales = WeeklyReport.getWeeklySalesInstance(salesID, salesCount);
				report = new WeeklyReport(reportID, beverageID, barID, weekStock, week, sales);
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw new RuntimeException("no WeeklyReport found vor bar-id " + barID + " and week " + week.toString());
		}
		controller.closeConnection();
		return report;
	}

	@Override
	public void addWeeklyReport(WeeklyReport report) {
		WeeklySales sales = report.getWeeklySales();
		try {
			String sql = "INSERT INTO WeeklyReport(id, bar_id, beverage_id, week_stock, year_week, year) VALUES ('"
					+ report.getId() + "','" + report.getBarID() + "','" + report.getBeverageID() + "',"
					+ report.getWeekStock().getValue() + "," + report.getWeek().getYearWeek() + ","
					+ report.getWeek().getYear() + ");";
			controller.executeUpdateSQL(sql);
			sql = "INSERT INTO WeeklySales(id, report_id, sales_count) VALUES('" + sales.getId() + "','"
					+ report.getId() + "'," + sales.getSoldCount().getValue() + ");";
			controller.executeUpdateSQL(sql);
		} catch (SQLException e) {
			throw new RuntimeException("Adding WeeklyReport failed");
		}

	}

	@Override
	public void updateWeeklySales(String salesID, Count soldCount) {
		try {
			String sql = "UPDATE WeeklySales SET sales_count =" + soldCount.getValue() + " WHERE id ='" + salesID
					+ "';";
			controller.executeUpdateSQL(sql);
		} catch (SQLException e) {
			throw new RuntimeException("Updating WeeklySales failed");
		}

	}

	@Override
	public void updateWeekReport(WeeklyReport report) {
		WeeklySales sales = report.getWeeklySales();
		try {
			String sql = "UPDATE WeeklyReport SET week_stock =" + report.getWeekStock().getValue() + " WHERE id ='"
					+ report.getId() + "';";
			controller.executeUpdateSQL(sql);
			sql = "UPDATE WeeklySales SET sales_count =" + sales.getSoldCount().getValue() + " WHERE id ='"
					+ sales.getId() + "';";
			controller.executeUpdateSQL(sql);
		} catch (SQLException e) {
			throw new RuntimeException("Updating WeeklyReport failed");
		}

	}

	@Override
	public boolean doesAnyWeekReportExist(String barID, Week week) {
		boolean found = false;
		String sql = "SELECT * FROM WeeklyReport WHERE bar_id = '" + barID + "' AND year = " + week.getYear()
				+ " AND year_week = " + week.getYearWeek() + ";";
		ResultSet rs = controller.executeSQL(sql);
		try {
			if (rs.next()) {
				found = true;
			}
		} catch (SQLException e) {
			controller.closeConnection();
			throw (new RuntimeException(e));
		}
		controller.closeConnection();
		return found;
	}

}
