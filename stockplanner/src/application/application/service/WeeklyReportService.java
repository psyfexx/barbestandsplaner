package application.application.service;

import java.util.List;

import domain.domain.entity.WeeklyReport;
import domain.domain.repository.WeeklyReportRepository;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class WeeklyReportService {
	private WeeklyReportRepository reportRepository;

	public WeeklyReportService(WeeklyReportRepository reportRepository) {
		this.reportRepository = reportRepository;
	}

	public void copyAllWeekReportsToNextWeek(String barID) {
		Week currentWeek = Week.getCurrentWeek();
		for (WeeklyReport report : getAllWeeklyReports(barID, currentWeek)) {
			WeeklyReport newReport = WeeklyReport.createCopyForNextWeek(report);
			reportRepository.addWeeklyReport(newReport);
		}
	}

	public boolean doesAnyWeekReportExist(String barID, Week week) {
		return reportRepository.doesAnyWeekReportExist(barID, week);
	}

	public List<WeeklyReport> getAllWeeklyReports(String barID, Week week) {
		return reportRepository.findAllWeeklyReports(barID, week);
	}

	public void addWeeklyReport(String barID, String beverageID, Week week, Count weekStock) {
		WeeklyReport report = WeeklyReport.createWeeklyReport(barID, beverageID, week, weekStock);
		reportRepository.addWeeklyReport(report);
	}

	public WeeklyReport getWeeklyReport(String barID, String beverageID, Week week) {
		return reportRepository.findWeeklyReport(barID, beverageID, week);
	}

	public void updateWeekStockOfWeeklyReport(String barID, String beverageID, Week week, Count weekStock) {
		WeeklyReport report = getWeeklyReport(barID, beverageID, week);
		report.setWeekStock(weekStock);
		reportRepository.updateWeekReport(report);
	}

	public void updateWeeklySales(String salesID, Count soldCount) {
		reportRepository.updateWeeklySales(salesID, soldCount);
	}
}
