package application.application.service.use_case;

import java.util.ArrayList;
import java.util.List;

import application.application.dto.WeekEvaluationDTO;
import application.application.service.BeverageInfoService;
import application.application.service.WeeklyReportService;
import domain.domain.entity.Beverage;
import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class ShowWeekEvaluationService {
	private BeverageInfoService beverageInfoService;
	private WeeklyReportService reportService;

	public ShowWeekEvaluationService(BeverageInfoService beverageInfoService, WeeklyReportService reportService) {
		super();
		this.beverageInfoService = beverageInfoService;
		this.reportService = reportService;
	}

	public List<WeekEvaluationDTO> getWeekEvaluations(String barID, Week week) {
		List<WeekEvaluationDTO> weekEvaluations = new ArrayList<>();
		for (WeeklyReport report : reportService.getAllWeeklyReports(barID, week)) {
			Beverage beverage = beverageInfoService.getBeverageForID(report.getBeverageID(), barID);
			Count recommendedWeekStock = getRecommendedWeekStock(report.getWeekStock(),
					report.getWeeklySales().getSoldCount());
			weekEvaluations.add(new WeekEvaluationDTO(beverage, report, recommendedWeekStock));
		}
		return weekEvaluations;
	}

	public static Count getRecommendedWeekStock(Count weekStock, Count soldCount) {
		int recommendedWeekStock = weekStock.getValue();

		double soldPercentage = (double) soldCount.getValue() / weekStock.getValue();
		if (soldPercentage < 0.25) {
			recommendedWeekStock = (int) (recommendedWeekStock * 0.25);
		} else if (soldPercentage < 0.5) {
			recommendedWeekStock = (int) (recommendedWeekStock * 0.5);
		} else if (soldPercentage > 1.5) {
			recommendedWeekStock = recommendedWeekStock * 2;
		} else if (soldPercentage > 1) {
			recommendedWeekStock = (int) (recommendedWeekStock * 1.5);
		} else if (soldPercentage > 0.85) {
			recommendedWeekStock = (int) (recommendedWeekStock * 1.25);
		}
		return new Count(recommendedWeekStock);
	}

}
