package domain.domain.value_object;

import java.util.Objects;

public final class Name {
	private final String title;
	private final String prename;
	private final String surname;

	public Name(String title, String prename, String surname) {
		this.title = title;
		this.prename = prename;
		this.surname = surname;
	}

	@Override
	public String toString() {
		return title + " " + prename + " " + surname;
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, prename, surname);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Name))
			return false;
		Name other = (Name) obj;
		return Objects.equals(title, other.title) && Objects.equals(prename, other.prename)
				&& Objects.equals(surname, other.surname);
	}

}
