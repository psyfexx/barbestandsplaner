package domain.test.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import domain.domain.entity.WeeklyReport;
import domain.domain.entity.WeeklySales;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class WeeklyReportTest {

	@Test
	public void testAddToWeeklySales() {
		WeeklySales sales = WeeklyReport.getWeeklySalesInstance("testSalesID", new Count(10));
		WeeklyReport report = new WeeklyReport("testReportID", "testBeverageID", "testBarID", new Count(100),
				Week.getCurrentWeek(), sales);
		report.addToWeeklySales(15);
		assertEquals(25, sales.getSoldCount().getValue());
	}

}
