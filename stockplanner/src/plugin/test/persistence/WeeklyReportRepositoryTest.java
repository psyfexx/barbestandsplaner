package plugin.test.persistence;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;
import plugin.plugin.persistence.WeeklyReportRepositoryImpl;

public class WeeklyReportRepositoryTest {

	@Test
	public void testFindWeeklyReport() {
		WeeklyReportRepositoryImpl repository = new WeeklyReportRepositoryImpl("./mockData/test");
		WeeklyReport report = repository.findWeeklyReport("barID", "beverageID", new Week(2022, 19));
		Object[] foundReport = new Object[] { report.getWeekStock(), report.getWeeklySales().getId(),
				report.getWeeklySales().getSoldCount() };
		Object[] expectedReport = new Object[] { new Count(15), "salesID", new Count(5) };

		assertArrayEquals(expectedReport, foundReport);
	}

}
