package domain.domain.value_object;

import java.util.Objects;

public final class Count {

	private final int count;

	public Count(int count) {
		if (count < 0) {
			throw new IllegalArgumentException("Count can't be less then zero");
		}
		this.count = count;
	}

	public int getValue() {
		return count;
	}

	@Override
	public int hashCode() {
		return Objects.hash(count);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Count))
			return false;
		Count other = (Count) obj;
		return count == other.count;
	}

}
