package plugin.plugin.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import domain.domain.entity.BeverageStorage;
import plugin.plugin.ui.panel.StockPlannerPanel;

public class SelectBeverageInfoListener implements ActionListener {

	private StockPlannerPanel stockPlannerPanel;

	public SelectBeverageInfoListener(StockPlannerPanel stockPlannerPanel) {
		this.stockPlannerPanel = stockPlannerPanel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JComboBox<?>) {
			JComboBox<BeverageStorage> choice = (JComboBox<BeverageStorage>) e.getSource();
			if (choice.getSelectedIndex() >= 0) {
				BeverageStorage storage = ((BeverageStorage) choice.getSelectedItem());
				stockPlannerPanel.showSelectedBeverage(storage);
			}
		}

	}

}
