package plugin.plugin.ui.panel;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import plugin.plugin.ui.listener.FillStockListener;
import plugin.plugin.ui.listener.ShowShoppingListListener;
import plugin.plugin.ui.listener.ShowWeeklyReportListener;

public class FooterButtonPane extends JPanel {

	private static final long serialVersionUID = 8968331769812610884L;
	private JButton weeklyReportButton;
	private JButton shoppingListButton;
	private JButton fillStockButton;

	public FooterButtonPane(StockPlannerPanel stockPlannerPanel) {
		setPreferredSize(new Dimension(500, 50));
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));

		weeklyReportButton = new JButton("Weekly Report");
		shoppingListButton = new JButton("Shopping List");
		fillStockButton = new JButton("Fill Stock");

		add(weeklyReportButton);
		add(Box.createRigidArea(new Dimension(150, 0)));
		add(shoppingListButton);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(fillStockButton);

		weeklyReportButton.addActionListener(new ShowWeeklyReportListener(stockPlannerPanel));
		shoppingListButton.addActionListener(new ShowShoppingListListener(stockPlannerPanel));
		fillStockButton.addActionListener(new FillStockListener(stockPlannerPanel));

		enableFillStockButton(false);
	}

	public void enableFillStockButton(boolean b) {
		fillStockButton.setEnabled(b);
	}

	public void enableShoppingListButton(boolean b) {
		if (!b)
			enableFillStockButton(b);
		shoppingListButton.setEnabled(b);

	}

}
