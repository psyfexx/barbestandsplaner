package application.application.service.use_case;

import java.util.List;

import application.application.service.BeverageInfoService;
import application.application.service.WeeklyReportService;
import domain.domain.entity.BeverageStorage;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class ManageBeverageInfoService {
	private BeverageInfoService beverageInfoService;
	private WeeklyReportService reportService;

	public ManageBeverageInfoService(BeverageInfoService beverageInfoService, WeeklyReportService reportService) {
		super();
		this.beverageInfoService = beverageInfoService;
		this.reportService = reportService;
	}

	public void addNewBeverageInfo(BeverageStorage storage, Count weekStock) {
		if (!beverageInfoService.doesBeverageStorageWithSameNameAndTypeExist(storage)) {
			reportService.addWeeklyReport(storage.getBarID(), storage.getBeverage().getId(), Week.getCurrentWeek(),
					weekStock);
			reportService.addWeeklyReport(storage.getBarID(), storage.getBeverage().getId(), Week.getCommingWeek(),
					weekStock);
			beverageInfoService.addNewBeverageStorage(storage);
		}
	}

	public List<BeverageStorage> getAllBeverageStorages(String barID) {
		return beverageInfoService.getAllBeverageStorages(barID);
	}

	public void updateBeverageInfo(BeverageStorage storage, Count weekStock) {
		if (beverageInfoService.updateBeverageStorage(storage)) {
			reportService.updateWeekStockOfWeeklyReport(storage.getBarID(), storage.getBeverage().getId(),
					Week.getCommingWeek(), weekStock);
		}
	}
}
