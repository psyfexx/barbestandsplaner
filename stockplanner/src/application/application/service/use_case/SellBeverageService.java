package application.application.service.use_case;

import application.application.service.BeverageInfoService;
import application.application.service.WeeklyReportService;
import domain.domain.entity.BeverageStorage;
import domain.domain.entity.WeeklyReport;
import domain.domain.entity.WeeklySales;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class SellBeverageService {
	private BeverageInfoService beverageInfoService;
	private WeeklyReportService reportService;

	public SellBeverageService(BeverageInfoService beverageInfoService, WeeklyReportService reportService) {
		this.beverageInfoService = beverageInfoService;
		this.reportService = reportService;
	}

	public void addSoldBeverage(BeverageStorage storage, int soldCount, Week week) {
		WeeklyReport report = reportService.getWeeklyReport(storage.getBarID(), storage.getBeverage().getId(), week);
		report.addToWeeklySales(soldCount);
		WeeklySales sales = report.getWeeklySales();
		reportService.updateWeeklySales(sales.getId(), sales.getSoldCount());

		Count stock = storage.getStock();
		Count newStock = new Count(stock.getValue() - soldCount);
		storage.setStock(newStock);
		beverageInfoService.updateStockForBeverageStorage(storage);

	}

}
