package adapter.adapter.week_evaluation;

public class WeekEvaluationRessource {
	private String beverageName;
	private int weekStock;
	private int soldCount;
	private int recommendedWeekStock;

	public WeekEvaluationRessource(String beverageName, int weekStock, int soldCount, int recommendedWeekStock) {
		super();
		this.beverageName = beverageName;
		this.weekStock = weekStock;
		this.soldCount = soldCount;
		this.recommendedWeekStock = recommendedWeekStock;
	}

	public String getBeverageName() {
		return beverageName;
	}

	public int getWeekStock() {
		return weekStock;
	}

	public int getSoldCount() {
		return soldCount;
	}

	public int getRecommendedWeekStock() {
		return recommendedWeekStock;
	}

}
