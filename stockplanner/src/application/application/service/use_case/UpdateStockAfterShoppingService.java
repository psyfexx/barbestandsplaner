package application.application.service.use_case;

import java.util.List;

import application.application.service.BeverageInfoService;
import application.application.service.WeeklyReportService;
import domain.domain.entity.BeverageStorage;
import domain.domain.entity.WeeklyReport;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class UpdateStockAfterShoppingService {
	private BeverageInfoService beverageInfoService;
	private WeeklyReportService reportService;

	public UpdateStockAfterShoppingService(BeverageInfoService beverageInfoService, WeeklyReportService reportService) {
		super();
		this.beverageInfoService = beverageInfoService;
		this.reportService = reportService;
	}

	public List<BeverageStorage> updateBeverageStorages(String barID) {
		List<BeverageStorage> storages = beverageInfoService.getAllBeverageStorages(barID);
		Week commingWeek = Week.getCommingWeek();
		for (BeverageStorage storage : storages) {
			updateStock(barID, storage, commingWeek);
		}
		return storages;
	}

	private void updateStock(String barID, BeverageStorage storage, Week commingWeek) {
		WeeklyReport report = reportService.getWeeklyReport(barID, storage.getBeverage().getId(), commingWeek);
		int currentStock = storage.getStock().getValue();
		int nextWeekStock = report.getWeekStock().getValue();
		int newStock = Math.max(currentStock, nextWeekStock);
		storage.setStock(new Count(newStock));
		beverageInfoService.updateStockForBeverageStorage(storage);
	}

}
