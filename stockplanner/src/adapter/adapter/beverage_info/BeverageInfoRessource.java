package adapter.adapter.beverage_info;

public class BeverageInfoRessource {
	private int stock;
	private double buyingPrice;
	private double sellingPrice;
	private String buyingPriceWithCurrency;
	private String sellingPriceWithCurrency;
	private String name;
	private String type;

	public BeverageInfoRessource(int stock, double buyingPrice, double sellingPrice, String buyingPriceWithCurrency,
			String sellingPriceWithCurrency, String name, String type) {
		super();
		this.stock = stock;
		this.buyingPrice = buyingPrice;
		this.sellingPrice = sellingPrice;
		this.buyingPriceWithCurrency = buyingPriceWithCurrency;
		this.sellingPriceWithCurrency = sellingPriceWithCurrency;
		this.name = name;
		this.type = type;
	}

	public int getStock() {
		return stock;
	}

	public double getBuyingPrice() {
		return buyingPrice;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public String getBuyingPriceWithCurrency() {
		return buyingPriceWithCurrency;
	}

	public String getSellingPriceWithCurrency() {
		return sellingPriceWithCurrency;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

}
