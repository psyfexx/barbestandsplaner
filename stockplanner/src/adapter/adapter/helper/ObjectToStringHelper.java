package adapter.adapter.helper;

import java.util.List;

import adapter.adapter.beverage_info.BeverageInfoRessource;
import adapter.adapter.beverage_info.BeverageInfoRessourceMapper;
import adapter.adapter.shopping_item.ShoppingItemRessource;
import adapter.adapter.shopping_item.ShoppingItemRessourceMapper;
import adapter.adapter.week_evaluation.WeekEvaluationRessource;
import adapter.adapter.week_evaluation.WeekEvaluationRessourceMapper;
import application.application.dto.ShoppingItemDTO;
import application.application.dto.WeekEvaluationDTO;
import domain.domain.entity.BeverageStorage;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;

public class ObjectToStringHelper {

	public static String getShoppingList(List<ShoppingItemDTO> shoppingItems, Cost totalCost) {
		ShoppingItemRessourceMapper mapper = new ShoppingItemRessourceMapper();

		StringBuilder sb = new StringBuilder();
		sb.append("SHOPPING LIST \n =================================\n");
		sb.append("Name" + getTab("Name", 3) + "Selling Price \t Number \t Selling Price total\n");

		for (ShoppingItemDTO shoppingItem : shoppingItems) {
			ShoppingItemRessource ressource = mapper.apply(shoppingItem);
			String beverageName = ressource.getBeverageName();
			sb.append(beverageName + ":" + getTab(beverageName, 4) + ressource.getSingleCost() + "\t "
					+ ressource.getCount() + "\t " + ressource.getTotalCost() + "\n");
		}
		sb.append("\n Total Cost: " + totalCost.toString());
		return sb.toString();
	}

	private static String getTab(String inputString, int tabCount) {
		int tabNumber = Math.floorDiv(tabCount * 8 - inputString.length(), 8);
		return "\t".repeat(tabNumber);
	}

	public static String getBeverageInfoForCurrentWeek(Count weekStock, BeverageStorage storage) {
		return getBeverageInfo(weekStock, storage, true);
	}

	public static String getBeverageInfoForPastWeek(Count weekStock, BeverageStorage storage) {
		return getBeverageInfo(weekStock, storage, false);
	}

	public static String getWeekEvaluationForCurrentWeek(List<WeekEvaluationDTO> evaluations) {
		return getWeekEvaluation(evaluations, true);
	}

	public static String getWeekEvaluationForPastWeek(List<WeekEvaluationDTO> evaluations) {
		return getWeekEvaluation(evaluations, false);
	}

	private static String getBeverageInfo(Count weekStock, BeverageStorage storage, boolean forCurrentWeek) {
		BeverageInfoRessourceMapper mapper = new BeverageInfoRessourceMapper();
		BeverageInfoRessource ressource = mapper.apply(storage);
		StringBuilder sb = new StringBuilder();
		sb.append("BEVERAGE INFO \n =================================\n");
		sb.append("Name: " + ressource.getName() + "\n");
		sb.append("Category: " + ressource.getType() + "\n");
		sb.append("Buying Price: " + ressource.getBuyingPriceWithCurrency() + "\n");
		sb.append("Selling Price: " + ressource.getSellingPriceWithCurrency() + "\n");
		if (forCurrentWeek) {
			sb.append("Current Stock: " + ressource.getStock() + "\n");
		}
		sb.append("Week Stock: " + weekStock.getValue());
		return sb.toString();
	}

	private static String getWeekEvaluation(List<WeekEvaluationDTO> evaluations, boolean forCurrentWeek) {
		WeekEvaluationRessourceMapper mapper = new WeekEvaluationRessourceMapper();

		StringBuilder sb = new StringBuilder();
		sb.append("WEEK EVALUATION \n =================================\n");
		sb.append("Name " + getTab("Name", 3) + "Week Stock \t Sold Count");
		if (forCurrentWeek) {
			sb.append("\t Recommended Week Stock\n");
		} else {
			sb.append("\n");
		}
		for (WeekEvaluationDTO evaluation : evaluations) {
			WeekEvaluationRessource ressource = mapper.apply(evaluation);
			String beverageName = ressource.getBeverageName();
			sb.append(beverageName + ":" + getTab(beverageName, 4) + ressource.getWeekStock() + "\t "
					+ ressource.getSoldCount());
			if (forCurrentWeek) {
				String recommendedStock = "" + ressource.getRecommendedWeekStock();
				if (ressource.getRecommendedWeekStock() - ressource.getWeekStock() == 0) {
					recommendedStock = "-";
				}
				sb.append("\t " + recommendedStock + "\n");
			} else {
				sb.append("\n");
			}
		}
		return sb.toString();
	}

}
