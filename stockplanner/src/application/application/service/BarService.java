package application.application.service;

import domain.domain.entity.Bar;
import domain.domain.repository.BarRepository;

public class BarService {
	private BarRepository barRepository;

	public BarService(BarRepository barRepository) {
		super();
		this.barRepository = barRepository;
	}

	public Bar getBar(String barID) {
		return barRepository.findBarForID(barID);
	}
}
