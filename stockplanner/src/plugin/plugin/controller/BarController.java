package plugin.plugin.controller;

import java.util.List;

import adapter.adapter.helper.ObjectToStringHelper;
import application.application.dto.ShoppingItemDTO;
import application.application.dto.WeekEvaluationDTO;
import application.application.service.BarService;
import application.application.service.BeverageInfoService;
import application.application.service.WeeklyReportService;
import application.application.service.use_case.ManageBeverageInfoService;
import application.application.service.use_case.SellBeverageService;
import application.application.service.use_case.ShowShoppingListService;
import application.application.service.use_case.ShowWeekEvaluationService;
import application.application.service.use_case.UpdateStockAfterShoppingService;
import domain.domain.entity.Bar;
import domain.domain.entity.BeverageStorage;
import domain.domain.entity.WeeklyReport;
import domain.domain.repository.BarRepository;
import domain.domain.repository.BeverageStorageRepository;
import domain.domain.repository.WeeklyReportRepository;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class BarController {
	private final String barID;
	private Week week;
	private final BarRepository barRepository;
	private WeeklyReportService reportService;
	private BeverageInfoService beverageService;

	public BarController(String barID, BarRepository barRepository, BeverageStorageRepository storageRepository,
			WeeklyReportRepository reportRepository, Week week) {
		super();
		this.barID = barID;
		this.barRepository = barRepository;
		this.week = week;
		this.reportService = new WeeklyReportService(reportRepository);
		this.beverageService = new BeverageInfoService(storageRepository);
		addCurrentWeekRepositoriesIfNecessary();
		addCommingWeekRepositoriesIfNecessary();
	}

	private void addCurrentWeekRepositoriesIfNecessary() {
		Week currentWeek = Week.getCurrentWeek();
		if (!reportService.doesAnyWeekReportExist(barID, currentWeek)) {
			for (BeverageStorage storage : beverageService.getAllBeverageStorages(barID)) {
				reportService.addWeeklyReport(barID, storage.getBeverage().getId(), currentWeek, storage.getStock());
			}
		}

	}

	private void addCommingWeekRepositoriesIfNecessary() {
		Week commingWeek = Week.getCommingWeek();
		if (!reportService.doesAnyWeekReportExist(barID, commingWeek)) {
			reportService.copyAllWeekReportsToNextWeek(barID);
		}
	}

	public Week getWeek() {
		return week;
	}

	public Bar getBar() {
		BarService barService = new BarService(barRepository);
		return barService.getBar(barID);
	}

	public WeeklyReport getWeeklyReport(BeverageStorage storage) {
		return reportService.getWeeklyReport(barID, storage.getBeverage().getId(), week);
	}

	public void changeWeek(boolean next) {
		if (next) {
			week = Week.getNextWeek(week);
		} else {
			week = Week.getLastWeek(week);
		}
	}

	public boolean doesLastWeekReportExist() {

		return reportService.doesAnyWeekReportExist(barID, Week.getLastWeek(week));
	}

	public List<BeverageStorage> getBeverageStorages() {
		ManageBeverageInfoService manageBeverageService = new ManageBeverageInfoService(beverageService, reportService);
		return manageBeverageService.getAllBeverageStorages(barID);
	}

	public List<BeverageStorage> fillStock() {
		UpdateStockAfterShoppingService stockService = new UpdateStockAfterShoppingService(beverageService,
				reportService);
		return stockService.updateBeverageStorages(barID);
	}

	public void sellBeverage(BeverageStorage storage, int count) {
		SellBeverageService sellService = new SellBeverageService(beverageService, reportService);
		sellService.addSoldBeverage(storage, count, week);
	}

	public String getShoppingList() {
		ShowShoppingListService shoppingService = new ShowShoppingListService(beverageService, reportService);
		List<ShoppingItemDTO> shoppingItems = shoppingService.getShoppingList(barID);
		Cost totalCost = shoppingService.getTotalCost(shoppingItems);
		return ObjectToStringHelper.getShoppingList(shoppingItems, totalCost);
	}

	public String getWeekEvaluation() {
		ShowWeekEvaluationService evaluationService = new ShowWeekEvaluationService(beverageService, reportService);
		List<WeekEvaluationDTO> evaluations = evaluationService.getWeekEvaluations(barID, week);
		if (week.equals(Week.getCurrentWeek())) {
			return ObjectToStringHelper.getWeekEvaluationForCurrentWeek(evaluations);
		} else {
			return ObjectToStringHelper.getWeekEvaluationForPastWeek(evaluations);
		}
	}

	public void createNewBeverage(BeverageStorage storage, Count weeklyStock) {
		ManageBeverageInfoService manageBeverageService = new ManageBeverageInfoService(beverageService, reportService);
		manageBeverageService.addNewBeverageInfo(storage, weeklyStock);
	}

	public void updateBeverage(BeverageStorage storage, Count weeklyStock) {
		ManageBeverageInfoService manageBeverageService = new ManageBeverageInfoService(beverageService, reportService);
		manageBeverageService.updateBeverageInfo(storage, weeklyStock);
	}

}
