package domain.test.strategy;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import domain.domain.enumeration.BeverageCategory;
import domain.domain.strategy.ProfitPriceStrategy;
import domain.domain.value_object.Cost;

public class ProfitPriceStrategyTest {
	private static Cost buyingPrice;
	private ProfitPriceStrategy strategy;

	@Before
	public void setup() {
		buyingPrice = new Cost(1.00);
		strategy = new ProfitPriceStrategy();
	}

	@Test
	public void testSuggestSellingPrice() {
		Cost sellingPrice = strategy.suggestSellingPrice(buyingPrice, BeverageCategory.WATER);
		assertEquals(new Cost(1.61), sellingPrice);
	}

	@Test
	public void testGetCostAfterTaxesForTypeWater() {
		Cost sellingPrice = strategy.getCostAfterTaxes(buyingPrice, BeverageCategory.WATER);
		assertEquals(new Cost(1.07), sellingPrice);
	}

	@Test
	public void testGetCostAfterTaxesForTypeOther() {
		Cost sellingPrice = strategy.getCostAfterTaxes(buyingPrice, BeverageCategory.WINE);
		assertEquals(new Cost(1.19), sellingPrice);
	}

}
