package domain.domain.strategy;

import domain.domain.enumeration.BeverageCategory;
import domain.domain.value_object.Cost;

public abstract class SellingPriceStrategy {

	public Cost getCostAfterTaxes(Cost buyingPrice, BeverageCategory category) {
		double price = buyingPrice.getValue();
		if (category.equals(BeverageCategory.WATER)) {
			price = price * 1.07;
		} else {
			price = price * 1.19;
		}

		return new Cost(price);
	}

	public abstract Cost suggestSellingPrice(Cost buyingPrice, BeverageCategory category);

}
