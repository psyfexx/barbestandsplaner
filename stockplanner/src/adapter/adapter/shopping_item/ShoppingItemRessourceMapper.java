package adapter.adapter.shopping_item;

import java.util.function.Function;

import application.application.dto.ShoppingItemDTO;
import domain.domain.entity.Beverage;

public class ShoppingItemRessourceMapper implements Function<ShoppingItemDTO, ShoppingItemRessource> {

	@Override
	public ShoppingItemRessource apply(ShoppingItemDTO item) {
		return map(item);
	}

	private ShoppingItemRessource map(ShoppingItemDTO item) {
		Beverage beverage = item.getBeverage();
		return new ShoppingItemRessource(beverage.getName(), beverage.getCategory().getName(),
				item.getNumber().getValue(), beverage.getBuyingPrice().getValue(), item.getTotalCost().getValue());
	}

}
