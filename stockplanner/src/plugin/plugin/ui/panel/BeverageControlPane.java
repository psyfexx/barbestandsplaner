package plugin.plugin.ui.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import domain.domain.entity.BeverageStorage;
import plugin.plugin.ui.listener.CreateBeverageListener;
import plugin.plugin.ui.listener.EditBeverageInfoListener;
import plugin.plugin.ui.listener.SelectBeverageInfoListener;
import plugin.plugin.ui.listener.SellBeverageListener;

public class BeverageControlPane extends JPanel {

	private static final long serialVersionUID = 1884999350425100650L;

	private JComboBox<BeverageStorage> beverageComboBox;
	private JButton createBeverageButton;
	private JButton editBeverageInfoButton;
	private JButton sellBeverageButton;
	private JFormattedTextField sellCountTextField;

	public BeverageControlPane(StockPlannerPanel stockPlannerPanel) {

		setPreferredSize(new Dimension(500, 50));
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));

		createBeverageButton = new JButton("+");
		editBeverageInfoButton = new JButton("\u270E");
		sellBeverageButton = new JButton("sell");
		sellCountTextField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		sellCountTextField.setColumns(5);
		beverageComboBox = new JComboBox<>();
		updateBeverageComboBox(stockPlannerPanel.getBeverages());

		add(new JLabel("Beverage: "));
		add(beverageComboBox);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(editBeverageInfoButton);
		add(Box.createRigidArea(new Dimension(5, 0)));
		add(createBeverageButton);
		add(Box.createRigidArea(new Dimension(50, 0)));
		add(sellCountTextField);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(sellBeverageButton);

		beverageComboBox.addActionListener(new SelectBeverageInfoListener(stockPlannerPanel));
		createBeverageButton.addActionListener(new CreateBeverageListener(stockPlannerPanel));
		editBeverageInfoButton.addActionListener(new EditBeverageInfoListener(stockPlannerPanel));
		sellBeverageButton.addActionListener(new SellBeverageListener(stockPlannerPanel));

		setOutOfFocus();
	}

	public void setOutOfFocus() {
		beverageComboBox.setSelectedIndex(-1);
		resetSellCountTextField();
		enableComponents(false);
	}

	public void resetSellCountTextField() {
		sellCountTextField.setValue(0);
	}

	public void enableComponents(boolean b) {
		editBeverageInfoButton.setEnabled(b);
		sellBeverageButton.setEnabled(b);
		sellCountTextField.setEnabled(b);
	}

	public void enableCreateButton(boolean b) {
		createBeverageButton.setEnabled(b);
	}

	public BeverageStorage getSelectedBeverage() {
		return (BeverageStorage) beverageComboBox.getSelectedItem();
	}

	public int getSellCount() {
		return ((Number) sellCountTextField.getValue()).intValue();
	}

	public void updateBeverageComboBox(List<BeverageStorage> storages) {
		beverageComboBox.removeAllItems();
		for (BeverageStorage storage : storages) {
			beverageComboBox.addItem(storage);
		}
	}

	public BeverageStorage getSelectedBeverageStorage() {
		return (BeverageStorage) beverageComboBox.getSelectedItem();

	}

	public void addNewBeverage(BeverageStorage storage) {
		beverageComboBox.addItem(storage);
		beverageComboBox.setSelectedItem(storage);

	}

}
