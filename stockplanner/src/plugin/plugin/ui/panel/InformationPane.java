package plugin.plugin.ui.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class InformationPane extends JPanel {
	private static final long serialVersionUID = 1919613551512747479L;
	private JTextArea infoBox = new JTextArea();

	public InformationPane() {
		setPreferredSize(new Dimension(500, 400));
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));

		infoBox.setEditable(false);
		JScrollPane scroll = new JScrollPane(infoBox);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 30));
		add(scroll, BorderLayout.CENTER);
		clearText();
	}

	public void setText(String text) {
		infoBox.setText(text);
	}

	public void clearText() {
		infoBox.setText("");
	}

}
