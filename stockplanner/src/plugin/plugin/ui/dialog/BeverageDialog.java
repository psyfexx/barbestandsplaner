package plugin.plugin.ui.dialog;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import domain.domain.entity.Beverage;
import domain.domain.entity.BeverageStorage;
import domain.domain.enumeration.BeverageCategory;
import domain.domain.enumeration.PriceMode;
import domain.domain.strategy.JustTaxesPriceStrategy;
import domain.domain.strategy.ProfitPriceStrategy;
import domain.domain.strategy.ToGoPriceStrategy;
import domain.domain.value_object.Cost;
import domain.domain.value_object.Count;
import domain.domain.value_object.Week;

public class BeverageDialog extends JPanel {

	private static final long serialVersionUID = 990031399447639553L;
	private JTextField nameField;
	private JComboBox<BeverageCategory> typeComboBox;
	private JComboBox<PriceMode> sellingModeComboBox;
	private JFormattedTextField buyingPriceField;
	private JFormattedTextField sellingPriceField;
	private JFormattedTextField weekStockField;
	private BeverageStorage storage;

	public BeverageDialog(BeverageStorage storage, Count weeklyStock, Week week) {
		super();
		initFields();
		Beverage beverage = storage.getBeverage();
		nameField.setText(beverage.getName());
		typeComboBox.setSelectedItem(beverage.getCategory());
		buyingPriceField.setValue(beverage.getBuyingPrice().getValue());
		sellingPriceField.setValue(beverage.getSellingPrice().getValue());
		weekStockField.setValue(weeklyStock.getValue());

		this.storage = storage;

		setupPanel(week);
	}

	public BeverageDialog(String barID) {
		super();
		initFields();
		Week week = Week.getCurrentWeek();
		nameField.setText("New Beverage");
		typeComboBox.setSelectedIndex(0);
		buyingPriceField.setValue(0.00);
		sellingPriceField.setValue(0.00);
		weekStockField.setValue(0);
		this.storage = BeverageStorage.createBeverageStorage(barID, new Count(0), nameField.getText(),
				(BeverageCategory) typeComboBox.getSelectedItem(), new Cost((double) buyingPriceField.getValue()),
				new Cost((double) sellingPriceField.getValue()));
		setupPanel(week);
	}

	public BeverageStorage getBeverageStorage() {
		Beverage oldBeverage = storage.getBeverage();
		Beverage newBeverage = BeverageStorage.getBeverageInstance(oldBeverage.getId(), oldBeverage.getBarId(),
				nameField.getText(), (BeverageCategory) typeComboBox.getSelectedItem(),
				new Cost(((Number) sellingPriceField.getValue()).doubleValue()),
				new Cost(((Number) buyingPriceField.getValue()).doubleValue()));
		storage.setBeverage(newBeverage);
		return storage;
	}

	public Count getWeeklyStock() {
		return new Count(((Number) weekStockField.getValue()).intValue());
	}

	private void initFields() {
		this.nameField = new JTextField();
		this.typeComboBox = new JComboBox<>();
		this.sellingModeComboBox = new JComboBox<>();
		this.buyingPriceField = new JFormattedTextField(new DecimalFormat("#0.00"));
		this.sellingPriceField = new JFormattedTextField(new DecimalFormat("#0.00"));
		this.weekStockField = new JFormattedTextField(NumberFormat.getIntegerInstance());

		for (BeverageCategory category : BeverageCategory.values()) {
			typeComboBox.addItem(category);
		}
		for (PriceMode mode : PriceMode.values()) {
			sellingModeComboBox.addItem(mode);
		}
		sellingModeComboBox.setSelectedIndex(0);

	}

	private void setupPanel(Week week) {
		setLayout(new GridLayout(6, 3));
		GridBagConstraints c = new GridBagConstraints();

		addElementToPanel(c, 1, 0, 0, new JLabel("Name"));
		addElementToPanel(c, 2, 1, 0, nameField);
		addElementToPanel(c, 1, 0, 1, new JLabel("Category: "));
		addElementToPanel(c, 2, 1, 1, typeComboBox);
		addElementToPanel(c, 2, 0, 2, new JLabel("Buying Price: "));
		addElementToPanel(c, 1, 2, 2, buyingPriceField);
		addElementToPanel(c, 1, 0, 3, new JLabel("Selling Mode: "));
		addElementToPanel(c, 2, 1, 3, sellingModeComboBox);
		addElementToPanel(c, 2, 0, 4, new JLabel("Selling Price: "));
		addElementToPanel(c, 1, 2, 4, sellingPriceField);
		addElementToPanel(c, 2, 0, 5, new JLabel("Week Stock for Week " + week.toString() + ": "));
		addElementToPanel(c, 1, 2, 5, weekStockField);

		buyingPriceField.getDocument().addDocumentListener(new BuyingPriceDocumentListener());
		sellingModeComboBox.addActionListener(a -> calculateSellingPrice());

	}

	private void addElementToPanel(GridBagConstraints constraints, int width, int x, int y, Component component) {
		constraints.gridwidth = width;
		constraints.gridx = x;
		constraints.gridy = y;
		add(component);
	}

	private void calculateSellingPrice() {
		String bPString = ((Number) buyingPriceField.getValue()).toString();
		Cost buyingPrice = new Cost(Double.valueOf(bPString));
		PriceMode mode = (PriceMode) sellingModeComboBox.getSelectedItem();
		BeverageCategory category = (BeverageCategory) typeComboBox.getSelectedItem();
		Cost sellingPrice = null;
		if (mode == PriceMode.JUST_TAXES) {
			sellingPrice = new JustTaxesPriceStrategy().suggestSellingPrice(buyingPrice, category);
		} else if (mode == PriceMode.WITH_PROFIT) {
			sellingPrice = new ProfitPriceStrategy().suggestSellingPrice(buyingPrice, category);
		} else if (mode == PriceMode.TO_GO) {
			sellingPrice = new ToGoPriceStrategy().suggestSellingPrice(buyingPrice, category);
		}
		sellingPriceField.setValue(sellingPrice.getValue());
	}

	private class BuyingPriceDocumentListener implements DocumentListener {

		@Override
		public void insertUpdate(DocumentEvent e) {
			calculateSellingPrice();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			calculateSellingPrice();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			calculateSellingPrice();
		}

	}

}
