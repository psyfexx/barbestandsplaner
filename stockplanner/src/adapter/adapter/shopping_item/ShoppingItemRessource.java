package adapter.adapter.shopping_item;

public class ShoppingItemRessource {
	private String beverageName;
	private String type;
	private int count;
	private double singleCost;
	private double totalCost;

	public ShoppingItemRessource(String beverageName, String type, int count, double singleCost, double totalCost) {
		super();
		this.beverageName = beverageName;
		this.type = type;
		this.count = count;
		this.singleCost = singleCost;
		this.totalCost = totalCost;
	}

	public String getBeverageName() {
		return beverageName;
	}

	public String getType() {
		return type;
	}

	public int getCount() {
		return count;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public double getSingleCost() {
		return singleCost;
	}

}
