package domain.domain.value_object;

import java.util.Calendar;

public final class Week {
	private final int year;

	private final int yearWeek;

	public Week(int year, int yearWeek) {
		this.year = year;
		this.yearWeek = yearWeek;
	}

	@Override
	public String toString() {
		return yearWeek + "|" + year;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Week))
			return false;
		Week other = (Week) o;
		return this.year == other.year && this.yearWeek == other.yearWeek;
	}

	@Override
	public final int hashCode() {
		return year + yearWeek * 10000;
	}

	public int getYear() {
		return year;
	}

	public int getYearWeek() {
		return yearWeek;
	}

	public static Week getCurrentWeek() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		int year = calendar.get(Calendar.YEAR);
		int yearWeek = calendar.get(Calendar.WEEK_OF_YEAR);
		return new Week(year, yearWeek);
	}

	public static Week getCommingWeek() {
		Week currentWeek = Week.getCurrentWeek();
		return getOtherWeek(currentWeek, true);
	}

	public static Week getNextWeek(Week week) {
		return getOtherWeek(week, true);
	}

	private static Week getOtherWeek(Week week, boolean next) {
		Calendar calendar = Calendar.getInstance();
		calendar.setWeekDate(week.getYear(), week.getYearWeek(), 2);
		long millis = calendar.getTimeInMillis();
		long weekMillis = 7 * 24 * 60 * 60 * 1000;
		if (next) {
			calendar.setTimeInMillis(millis + weekMillis);
		} else {
			calendar.setTimeInMillis(millis - weekMillis);
		}
		int year = calendar.get(Calendar.YEAR);
		int yearWeek = calendar.get(Calendar.WEEK_OF_YEAR);
		return new Week(year, yearWeek);
	}

	public static Week getLastWeek(Week week) {
		return getOtherWeek(week, false);
	}

}
