package domain.domain.enumeration;

public enum BeverageCategory {

	BEER("Bier", true), JUICE("Fruchtsaft", false), WINE("Wein", true), WATER("Wasser", false);

	private String name;
	private boolean alcoholic;

	private BeverageCategory(String name, boolean alcoholic) {
		this.name = name;
		this.alcoholic = alcoholic;
	}

	public String getName() {
		return name;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public static BeverageCategory getCategoryForName(String name) {
		BeverageCategory result = null;
		for (BeverageCategory category : BeverageCategory.values()) {
			if (name.equals(category.getName())) {
				result = category;
				break;
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		return name;
	}

}
